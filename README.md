# Girderstream Alpine Test

A simple example of how one might stage an alpine tarball as a girderstream stage0.
The provided script will perform the following steps;

- Download a minimal rootfs alpine tarball.
- Extract it and store it in a local buildbox cas running on localhost:50040.
- Take the digest of the uploaded alpine artifact and use it to create a girderstream artifact.
- Run girderstream to exsure the artifact can be staged in a sandbox correctly.

## Prerequisites

```code=bash
# Basic dependencies (debian 12)
sudo apt-get install build-depends vim git curl bubblewrap \
cmake g++ gcc googletest libgmock-dev libgoogle-glog-dev libssl-dev pkg-config uuid-dev \
grpc++ libprotobuf-dev protobuf-compiler-grpc

# buildbox-common
git clone https://gitlab.com/BuildGrid/buildbox/buildbox-common.git
pushd buildbox-common
mkdir build && cd build
cmake ..
make -j$(nproc)
sudo make install
popd

# buildbox-casd
git clone https://gitlab.com/BuildGrid/buildbox/buildbox-casd.git
pushd buildbox-casd
mkdir build && cd build
cmake ..
make -j$(nproc)
sudo make install
popd

# buildbox-casd.service (runs buildbox-casd for the system at boot)
sudo cp -v ./buildbox-casd.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now buildbox-casd.service

# buildbox-run-bubblewrap
git clone https://gitlab.com/BuildGrid/buildbox/buildbox-run-bubblewrap.git
pushd buildbox-run-bubblewrap
mkdir build && build
cmake ..
make -j$(nproc)
sudo make install
popd

# buildbox-tools (casupload)
git clone https://gitlab.com/BuildGrid/buildbox/buildbox-tools.git
pushd buildbox-tools
mkdir build && cd build
cmake ../cpp
make -j$(nproc)
sudo make install
popd

# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# girderstream (https://girderstream.gitlab.io/girderstream/book/main/install.html)
mkdir -vp ~/.local/bin
. ~/.profile
git clone https://gitlab.com/girderstream/girderstream.git
pushd girderstream
cargo build --release
cp target/release/girderstream ~/.local/bin/
popd
```

## Do things with Girderstream
```
./girder_things.sh
```
