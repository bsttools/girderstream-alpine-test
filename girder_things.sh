#!/bin/bash -ex

# populate a buildbox cas, and use girderstream to stage it in a sandbox.
# This script assumes a cas server is running on localhost:50040

ALPINE_VERSION=3.18.2
ALPINE_VERSION_MAJOR=${ALPINE_VERSION%.*}
ALPINE_URL=https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION_MAJOR}/releases/x86_64/alpine-minirootfs-${ALPINE_VERSION}-x86_64.tar.gz
ALPINE_SHA=6c0be6213d2718087e1f4e7847e711cea288dd6cbd92c436af2c22756ac7db53
ALPINE_FILENAME=alpine-${ALPINE_VERSION}.tar.gz

if [ ! -f ${ALPINE_FILENAME} ] ; then
	wget ${ALPINE_URL} -O ${ALPINE_FILENAME}
fi

# ensure the archive has downloaded correctly
echo ${ALPINE_SHA} ${ALPINE_FILENAME} | sha256sum --check

mkdir -vp alpine

# extract the tarball.
if [ ! -f alpine/etc/os-release ] ; then
	pushd alpine
	tar -xzf ../${ALPINE_FILENAME} 
	popd
fi

# ensure the files on disk match the version we expect.
grep "VERSION_ID=${ALPINE_VERSION}" alpine/etc/os-release

# upload the stage0 to the cas
casupload --cas-server=http://localhost:50040 --output-digest-file=digest alpine

# produce a stage0 element using this digest
DIGEST=$(cat digest);
sed -e "s#<STAGE0_DIGEST>#${DIGEST}#" \
    elements/stage0.grd.in > elements/stage0.grd

# run girderstream to see if it works.
girderstream -v build --name stage0

